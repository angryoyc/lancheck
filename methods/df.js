'use strict';

const { Method } = require('../modules/method_class');

const { exec }    = require('../modules/commands_common');

const config      = require('../config');


class Df extends Method {
	constructor(){
		super();
		this.description = "Тестирование удалённого хоста (под управлением unix-подобной os) на свободное место на блочных устройствах";
		this.props = [
			{prop: "ssh",        desc:"адрес хоста, доступного по ssh, на котором будут тестироваться блочные устройства. Это может быть как ip-адрес, так и символьное имя хоста", type: 'string' },
			{prop: "blk",        desc:"Имя блочного устройства. Например, /dev/sda1", type: 'string' },
			{prop: "threshold",  desc:"Порог нормы. Процент занятости, выше которого тест считается проваленым. По умолчанию 80%", type: 'number' }
		];
	}

	toString(conf){ // Обязательный метод toString - возвращает строку-представление по объекту-конфигурации соответствующего типа
		return 'Check free space on ' + conf.ssh + ':' + (conf.port || '') + conf.blk  + '  ( ' + (conf.note || '') + ' )';
	};

	async check(conf){  // Обязательный метод check
		try{
			if( !conf.ssh ) throw new Error('Неверный параметр: адреса ssh-хоста');
			let cmd = 'ssh';
			if(config.sys && (cmd in config.sys) ) cmd = config.sys[cmd];
			let lines = await exec( cmd, [conf.ssh, 'df'] );
			lines = lines.filter(function(r){return r;}).filter(function(r){return r.match(conf.blk);});
			const resstring = lines.filter(function(l){
				return l.match(/(.+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\%/);
			})[0];
			if(resstring){
				const m = resstring.match(/(.+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\%/);
				let threshold = conf.threshold || 80;
				if( (1*m[5]) > (1*threshold) ){
					throw new Error('Процент использованного пространства превышен. Использовано: ' + m[5] + '% Допустимо до: ' + threshold + '%' );
				}else{
					return { status:'ok' };
				};
			}else{
				throw new Error('Формат выдаваемых сообщений не распознан');
			};
		}catch(e){
			return { status:'error', message: e.message };
		};
	}

}

module.exports = new Df();
