'use strict';

const { Method } = require('../modules/method_class');

class Point extends Method {
	constructor(){
		super();
		this.description = "Специальный метод. Ничего не тестирует. В ключается в дерево узлов для указания точки выхода во внешний мир"; // Обязательное свойство description - возвращает краткое (одной строкой) описание метода
		this.props = [
			{ prop: "cmd",  desc: "Команда, которая будет выполняться, при попытке отправки оповещений через эту точку выхода. Доступны макроподстановки, записываемые в фигурных скобках." }
		]
	}

	toString(conf){ // Обязательный метод toString - возвращает строку-представление по объекту-конфигурации соответствующего типа
		return 'Точка выхода, для отправки сообщений ' + conf.cmd + ' )';
	};

	async check(conf){
		return  {status:'ok'};
	}

}

module.exports = new Point();
