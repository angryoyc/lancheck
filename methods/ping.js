'use strict';

const { Method } = require('../modules/method_class');

const spawn = require('child_process').spawn;
const config = require('../config');
const count_default = 2;
const threshold_default = 50;

class Ping extends Method {
	constructor(){
		super();
		this.description = "Тестирование удалённого хоста по протоколу ICMP (ping)."; // Обязательный метод descr - возвращает краткое (одной строкой) описание метода
		this.props = [
			{prop: "ip",        desc:"адрес тестируемого хоста. Это может быть как ip-адрес, так и символьное имя хоста" },
			{prop: "threshold", desc:"порог. Процент неуспешных пингов, для того чтобы признать тест проваленым. По умолчанию " + threshold_default + "%" },
			{prop: "count",     desc:"количество ping-пакетов. Увеличение количества пакетов увеличивает достоверность тестирования, но и увеличивает время. По умолчанию - " + count_default }
		]
	}

	toString( conf ){ // Обязательный метод toString - возвращает строку-представление по объекту-конфигурации соответствующего типа
		return 'PING ' + conf.ip  + '  ( ' + (conf.note || '') + ' )';
	};

	async check(conf){ // Обязательный метод check
		return new Promise(function( resolve, reject ){
			const params = [ conf.ip, '-c', (conf.count || count_default) ];
			const threshold = (conf.threshold || threshold_default);
			const appname = (config.sys?config.sys.ping:'') || 'ping';
			try{
				const ping = spawn( appname, params );
				const rows = [];
				const err_mess = [];
				ping.stdout.on('data', (r) => {
					rows.push(r.toString());
				});
				ping.stderr.on('data', (r) => {
					err_mess.push(r.toString());
				});
				ping.on('close', function(code){
					const lines = rows.join('').split(/\n/).filter(function(r){return r;});
					const resstring = lines.filter(function(l){
						return l.match(/packets transmitted.+\d+\% packet loss/);
					})[0];
					if(resstring){
						const m = resstring.match(/(\d+)\% packet loss/);
						if( (1*m[1]) > (1*threshold) ){
							resolve({status:'error', message: 'Выявлено превышение порога ошибок. Порог: ' + threshold + '% Выявленное значение:' + m[1] + '%'} );
						}else{
							resolve({status:'ok'});
						};
					}else{
						resolve({status:'error', message: 'Сбой при проведении теста: Формат сообщений внешней утилиты не распознан' });
					};
				});
			}catch(e){
				resolve({status:'error', message: 'Сбой при проведении теста: ' + e.message });
			};
		});
	}
}

module.exports = new Ping();
