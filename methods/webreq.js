'use strict';

//exports.__proto__ = require('../modules/method_prototype');
const { Method } = require('../modules/method_class');

const axios = require('axios');
const util = require('util');
const urlparser = require('url');
const timeout_default = 3000;

class Webreq extends Method {
	constructor(){
		super();
		this.description = "Тестирование удалённого хоста на доступность по протоколам http/https"; // Обязательный метод descr - возвращает краткое (одной строкой) описание метода
		this.props = [
			{prop: "method",       desc: "HTTP-Метод: POST|GET" },
			{prop: "url",          desc: "Запрашиваемый адрес (url)" },
			{prop: "username",     desc: "Имя пользователя" },
			{prop: "password",     desc: "Пароль" },
			{prop: "timeout",      desc: "Таймаут запроса" },
			{prop: "skiphtmltest", desc: "Если true - пропустить тест текста HTML полученного от хоста, иначе ответ должен начинаться на \"<html\" или \"<!DOCTYPE\" " }
		]
	}

	toString(conf){ // Обязательный метод toString - возвращает строку-представление по объекту-конфигурации соответствующего типа
		return 'HTTP(s) Reguest ' + conf.url  + '  ( ' + (conf.note || '') + ' )';
	};

	async check(conf){  // Обязательный метод check
		try{
			const method = ( conf.method || 'get' ).toLowerCase();
			const skiphtmltest = ( conf.skiphtmltest || false );
			const timeout = ( conf.timeout || 3000 );
			let url = ( conf.url || '' ).toLowerCase();
			if( !url ) throw new Error('Пустой URL');
			const parsed = urlparser.parse( url );
			if( parsed.auth ){
				let a = parsed.auth.split(/\:/);
				conf.username = conf.username || a[0];
				conf.password = conf.password || a[1];
				parsed.auth=null;
				url = conf.url = urlparser.format(parsed, {auth:false});
			}
			const options = { url, method };
			if( conf.username || conf.password ){
				if( !options.auth ) options.auth = {};
				if( conf.username ) options.auth.username = conf.username;
				if( conf.password ) options.auth.password = conf.password;
			}
			if( conf.data ) options.data = conf.data;
			options.timeout = timeout;
			const result = await axios( options );
			if( result.headers && result.headers['content-type'] && (result.headers['content-type']=='text/html') && (!skiphtmltest) ){
				let data = result.data
				.replace(/\n/mg,'')
				.replace(/\r/mg,'')
				.replace(/^ +/,'');
				if( data.match(/^.{0,1}\<\!DOCTYPE/i) || data.match(/^.{0,1}\<html/i) ){
					return { status:'ok' };
				}else{ 
					throw new Error('Некорректный ответ: '  + data.substr(0, 20) + '...' );
				}
			}else{
				return { status:'ok' };
			}
		}catch(e){
			return { status:'error', message: e.message };
		};
	}
}

module.exports = new Webreq();
