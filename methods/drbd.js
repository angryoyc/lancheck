'use strict';

const { Method } = require('../modules/method_class');

const { exec }   = require('../modules/commands_common');

const config     = require('../config');

class Drbd extends Method {
	constructor(){
		super();
		this.description = "Тестирование статуса сервиса DRBD (программный сетевой raid) на удалённом хосте"; // Обязательный метод descr - возвращает краткое (одной строкой) описание метода
		this.props = [
			{prop: "ssh",        desc:"адрес хоста, доступного по ssh, на котором будет тестироваться состояние DRBD-подсистемы", type: 'string' }
		]
	}

	toString( conf ){ // Обязательный метод toString - возвращает строку-представление по объекту-конфигурации соответствующего типа
		return 'DRBD-Status on host ' + conf.ssh  + '  ( ' + (conf.note || '') + ' )';
	};

	async check(conf){  // Обязательный метод check
		try{
			if( !conf.ssh ) throw new Error('Неверный параметр: адреса ssh-хоста');
			let cmd = 'ssh';
			if( config.sys && ( cmd in config.sys ) ) cmd = config.sys[ cmd ];
			let lines = await exec( cmd, [conf.ssh, 'cat /proc/drbd | grep [0-9]\:'] );
			//console.log(lines);
			var ok_counter = lines.filter(function(s){
				return s.match(/UpToDate\/UpToDate/);
			}).length;
			if(ok_counter < lines.length){
				return { status:'error', message: 'Не все DRBD-ресурсы находятся в синхронизинованном состоянии' };
			}else{
				return { status:'ok' };
			}
		}catch(e){
			return { status:'error', message: e.message };
		};
	}

}

module.exports = new Drbd();




