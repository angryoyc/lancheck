'use strict';

const require_tree = require('require-tree');

module.exports = function( conf_path, ENV ){
	const conf = require( conf_path || '../config.json');
	delete conf.current_nodes;
	let commands = require_tree('../commands');
	let methods = require_tree('../methods');
	for( let mtype in methods ) methods[mtype].set_method_type(mtype);
	methods.ENV = ENV;
	for(let c in commands){
		this[c] = (commands[c])( conf, methods, conf_path );
	}
};

