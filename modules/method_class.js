const ESC           = '\x1b[';

exports.Method = class {
	details({nocolor=false}){
		const props = this.props || [];
		let lines = [];
		for(let p of props){
			lines.push(' '.repeat( 2 ) + (!nocolor?ESC+'32m':'') + '- ' + (!nocolor?ESC+'0m':'') + (!nocolor?ESC+'36m':'') + p.prop + (!nocolor?ESC+'0m':'') + ' '.repeat(15-p.prop.length) + ' - ' + p.desc);
		}
		return lines.join('\n');
	};

	desc({nocolor=false}){
		return (!nocolor?ESC+'36m':'') +  this.methodtype + (!nocolor?ESC+'0m':'')  + ' - '  + this.description || '-- без описания --'; 
	};

	set_method_type(method_type){
		this.methodtype = method_type;
	}

};


