'use strict';

const ESC         = '\x1b[';
const { print }   = require('../modules/print_methods');
const isObject    = function(a){return (!!a) && (a.constructor === Object)};
const isArray     = function(a){return (!!a) && (a.constructor === Array);};
const spawn             = require('child_process').spawn;

exports.check_nodes = async function( methods, nodes, treesimbol, callback){
	if( !isArray(nodes) ) throw new Error('Список узлов не загружен. Используйте команду "load"');
	let cont = true;
	for(let i in nodes){
		let n = nodes[i];
		let last  =  ((nodes.length - 1) == i);
		if( typeof(callback) == 'function'){
			cont = await callback( n, treesimbol + (last?String.fromCharCode('0x2514'):String.fromCharCode('0x251C')) );
		}
		if( (( n.nodes || [] ).length>0) && cont && methods.ENV.maycontinue ){
			await exports.check_nodes( methods, n.nodes, treesimbol + (last?'  ':String.fromCharCode('0x2502')+' '), callback );
		}
		if( !methods.ENV.maycontinue ) break;
	};
	return cont;
}

exports.print_status = function (n, { nocolor=false }){
	print( exports.status_string( n, {nocolor}), 0 );
}

exports.status_string = function (n, { nocolor=false }){
	const result = [' [ '];
	if( n.laststate ){
		let laststate = {};
		if( typeof(n.laststate)=='string' ){
			let m = n.laststate.split(/\:/);
			laststate.status = m[0].toLowerCase();
			laststate.message = m[1];
		}else if( isObject(n.laststate) ){
			laststate = n.laststate;
		}
		if( (laststate.status || '').match(/^err/) ){
			if( !nocolor ) result.push( ESC + '31m' );
			result.push( 'ERR: ' );
			if( !nocolor ) result.push( ESC + '0m' );
			if( !nocolor ) result.push( ESC + '1m' );
			result.push( laststate.message );
			if( !nocolor ) result.push( ESC + '0m' );
		}else if( (laststate.status || '').match(/^ok/) ){
			if( !nocolor ) result.push( ESC + '32m' );
			result.push( 'OK' );
			if( !nocolor ) result.push( ESC + '0m' );
		}else{
			if( !nocolor ) result.push( ESC + '33m' );
			result.push( laststate.status || 'не тестировалось' );
			if( !nocolor ) result.push( ESC + '0m' );
		}
	}else{
		if( !nocolor ) result.push( ESC + '33m' );
		result.push(n.laststate || 'не тестировалось' );
		if( !nocolor ) result.push( ESC + '0m' );
	}
	result.push( ' ]' );
	return result.join('');
}

exports.exec = async function( binname, params, data){
	return new Promise(( resolve, reject )=>{
		const bin = spawn( binname, params );
		const stderr_data = [];
		const stdout_data = [];
		bin.stdout.on('data', ( data ) => { stdout_data.push(data.toString('utf8')); });
		bin.stderr.on('data', ( data ) => { stderr_data.push(data.toString('utf8')); });
		bin.on('close', ( code ) => {
			let a;
			if( code ){
				a = stderr_data.join('').split(/\r*\n/);
				if(a.length>0){
					reject(new Error(a[0]));
				}else{
					reject(new Error('Неизвестная ошибка'));
				}
			}else{
				//ok
				a = stdout_data
				.join('')
				.split(/\r*\n/)
				.filter( s=>{return s} );
				resolve( a );
			}
		});
		if( data ){
			bin.stdin.write(data);
			bin.stdin.end();
		}
	});
}
