let list = null;

exports.clear = function(){
	list = null;
}

exports.push = function( el ){
	if( !list ) list = [];
	list.push( el );
}

exports.get = function(){
	return list || [];
}
exports.sort = function(){
	if(list){
		list = list.sort( (a,b) =>{
			return (( a.order || 0 ) + (a.isok?0:100)) - ((b.order || 0 )  + (b.isok?0:100));
		} );
	};
}
