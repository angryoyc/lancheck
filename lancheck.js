#!/usr/bin/node
'use strict';

const ENV = {};
const confpath          = __dirname + '/config.json';
const lancheck          = new (require('./modules/lancheck_lib'))(confpath, ENV);
const chalk             = require('chalk');
const figlet            = require('figlet');
const ESC               = '\x1b[';
const pkg               = require('./package');
const program           = require('./modules/commander_async');
const { print }         = require('./modules/print_methods');
const { print_object }  = require('./modules/print_methods');
const points            = require('./modules/points');

/*
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);
const readFile = promisify(fs.readFile);

const url = require('url');
const path = require('path');
const isArray     = function(a){return (!!a) && (a.constructor === Array);};
const isObject    = function(a){return (!!a) && (a.constructor === Object)};
*/

program
	.name(pkg.name)
	.version(pkg.version)
	.description( 'Интерфейс коммандной строки для конфигурирования и запуска проверок сетевых устройств.');

//                                                                =========== MAIN =============
program
	.command('ls')
	.description('Вывод списка тестов')
	.option( '-s', '--status', 'Выводить информацию о результате тестирования', null, 'false' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-e', '--error', 'Выводить информацию только о неуспешных задачах', null, 'false' )
	.option( '-c', '--changed', 'Выводить информацию только о задачах сменивших состояние с "ok" на любое другое', null, 'false' )
	.option( '-t', '--time', 'Выводить информацию о времени теста', null, 'false' )
	.action( async function(cmd, argv){
		argv.status = !!('-s' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.changed = !!('-c' in cmd.options);
		argv.error = !!('-e' in cmd.options);
		argv.time = !!('-t' in cmd.options);
		await lancheck.ls( argv );
	});

program
	.command('start')
	.alias('go')
	.description('Запуск тестов')
	.long('[<nodelist>]')
	.option( '-a', '--alert', 'Запускать процедуру оповещения после тестирования. Аналогично alert. (см. help alert)', null, 'false' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--force', 'Тестировать узлы, зависимые от узлов, не прошедших тест', null, 'false' )
	.option( '-s', '--save', 'Сохранить результаты тестировани в conf-файле', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения', null, 'false' )
	.option( '-t', '--total', 'После тестирования вывести список изменённых узлв. Аналогично ls -c -s -t', null, 'false' )
	.action( async function(cmd, argv){
		if( cmd.values && cmd.values['nodelist'] ) argv.nodelist = cmd.values['nodelist'];
		argv.force = !!('-f' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.savenodes = !!('-s' in cmd.options);
		argv.alert = !!('-a' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		argv.total = !!('-t' in cmd.options);
		await lancheck.check( argv );
		if( argv.total ){
			argv.changed = argv.status = argv.time = true;
			await lancheck.ls( argv );
		}
		if(argv.alert){
			argv.force = true; // Теперь этот параметр будет использоваться для вызова alert, а при вызове её из команды старт force всегда = true
			await lancheck.alert(argv);
		}
	});

program
	.command('alert')
	.description('Запуск оповещения')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--force', 'Отправлять сформированные оповещения', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения, кроме экстренных', null, 'false' )
	.note('Если последний тест списка узлов закончился хотябы одним новым неудачным тестом, то запускается процедура оповещения администраторов, основанная на списке админов, типе сбоев ("failtype") и их уровне. Для каждого админа задается список ограничений - это failtype с необязательным минимальным уровнем событий, о которых подлежит оповещать. Например, если для админа указано ограничение "video:3" - то админу будут отправляться опопвещения обо всех событиях, но события типа video, будут отправляться только если они имеют уровень 3 и выше.')
	.action( async function(cmd, argv){
		argv.force = !!('-f' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		await lancheck.alert(argv);
	});

program
	.command('clear')
	.description('Очистить узлы от результов тестирования')
	.option( '-s', '--save', 'Сохранить состояние узлов после очистки', null, 'false' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-o', '--ok', 'Очистить узлы, установив статус = ok', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения', null, 'false' )
	.action( async function(cmd, argv){
		argv.savenodes = !!('-s' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.ok = !!('-o' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		await lancheck.clear(argv);
	});

// ---------------------- ADMIN LIST -----------------------

program
	.command('addadmin')
	.alias('adduser')
	.description('Добавление админа')
	.long('<fio>')
	.action( async function(cmd, argv){
		if( cmd.values && cmd.values['fio'] ) argv.fio = cmd.values['fio'];
		await lancheck.admin_add( argv );
	});

program
	.command('admin')
	.description('Управление списком администраторов')
	.long('<rownum>')
	.action( async function(cmd, argv){
		if( cmd.values && cmd.values['rownum'] ) argv.rownum = cmd.values['rownum'];
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			if(argv.rownum){
				await lancheck.admin_info( argv );
			}else{
				await lancheck.admin_ls( argv );
			}
		}
	});

program
	.command('admin')
	.command('del')
	.description('Удаление админа.')
	.action( async function(cmd, argv){
		await lancheck.admin_del( argv );
	});

program
	.command('admin')
	.command('filter')
	.description('Задание ограничений на оповещение админа')
	.long('<filter> <level>')
	.note('Фильтры задают какие события будут отправлены данному админу. Например фильтр "lan:2" указывает на то, что админу будут отправлены оповещения о  сбоях типа lan и уровнем 2 и выше. Фильтр "*:0" - отправка всех событий. С этим фильтром по умолчанию создаётся админ.')
	.action( async function(cmd, argv){
		if( cmd.values && cmd.values['filter'] ) argv.filter = cmd.values['filter'];
		if( cmd.values && cmd.values['level'] ) argv.level = cmd.values['level'];
		await lancheck.admin_filters( argv );
	});

program
	.command('admin')
	.command('contact')
	.description('Задание контакта')
	.long('<contname> <contval>')
	.action( async function(cmd, argv){
		if( cmd.values && cmd.values['contname'] ) argv.contname = cmd.values['contname'];
		if( cmd.values && cmd.values['contval'] ) argv.contval = cmd.values['contval'];
		await lancheck.admin_contact( argv );
	});

program
	.command('admin')
	.command('activate')
	.description('Активирование админа')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		argv.tostate = true;
		await lancheck.admin_activate( argv );
	});

program
	.command('admin')
	.command('deactivate')
	.description('Деактивирование админа')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		argv.tostate = false;
		await lancheck.admin_activate( argv );
	});

// ---------------------------------------------------------

// ---------------------- POINTS -----------------------
program
	.command('points')
	.description('Вывести список обнаруженных в последний раз точек выхода во внешний мир')
	.action( async function(cmd, argv){
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			print_object( (points.get() || []).map( p => {return p.cmd + '::' + p.order + ' :: ' + p.isok} ));
		}
	});

program
	.command('points')
	.command('scan')
	.alias('rescan')
	.description('Пересканировать узлы и найти доступные точки')
	.action( async function(cmd, argv){
		await lancheck.points_rescan( argv );
		print_object( (points.get() || []).map( p => {return p.cmd + '::' + p.order + ' :: ' + p.isok} ) );
	});

// ---------------------------------------------------------

// ---------------------- NODES LIST -----------------------

program
	.command('load')
	.description('Загрузка списка тестируемых узлов')
	.long('[<nodelist>]')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		if( cmd.values && cmd.values['nodelist'] ) argv.nodelist = cmd.values['nodelist'];
		if( argv.nodelist ){
			await lancheck.load_if_not( argv );
		}else{
			await lancheck.nodes_ls( argv );
		}
	});

program
	.command('save')
	.description('Сохранить загруженный список узлов')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		await lancheck.save( {savenodes:true, nocolor:argv.nocolor} );
	});

program
	.command('nodes')
	.description('Работа со списками узлов')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.note('Список узлов - отдельный файл в формате JSON, хранящийся в папке, задаваемой командой "nodes path". Предавляет собой иерархический список (массив) узлов - такой список, каждый элемент которого может содержать параметр nodes, который сам указывает на массив дочерних узлов. Каждый узел задаёт один тест какого либо вида и его параметры. Например, для узлов типа "ping" задаётся параметр "ip", определяющий хост в сети, который и будет тестироваться на доступность. ')
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			await lancheck.nodes_ls( argv );
		}
	});

program
	.command('nodes')
	.command('path')
	.long('[<path>]')
	.description('Задать путь к папке списков узлов')
	.note('<path> - путь к папке со списками узлов. Только .json-файлы этой директории  (исключая вложенные директории) будут расцениваться как список тестируемых узлов.')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		if( cmd.values && cmd.values['path'] ) argv.nodes_path = cmd.values['path'];
		if(argv.nodes_path){
			await lancheck.set_conf( argv );
		}else{
			throw new Error('Путь не может быть пустым');
		}
	});
//                                                                =========== METHODS =============

program
	.command('methods')
	.description('Список доступных методов')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--full', 'Выводить полную информацию по всем опциям методов', null, 'false' )
	.action( async function(cmd, argv){
		argv.nocolor = !!('-n' in cmd.options);
		argv.full = !!('-f' in cmd.options);
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			await lancheck.methods_ls( argv );
		}
	});


//                                                                =========== OTHER =============
program
	.command('help')
	.alias('man')
	.long( '[<commandname>] [<commandname2>] [<commandname3>] [<commandname4>] [<commandname5>]' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--full', 'Полный help по всем командам', null, 'false' )
	.option( '-m', '--md', 'Выводить используя разметку md', null, 'false' )
	.description('Вывод короткой справки по командам CLI')
	.action( async function(cmd){
		const color = !('-n' in cmd.options);
		const full = !!('-f' in cmd.options);
		const md = !!('-m' in cmd.options);
		const style = md?2:(color?1:0);
		if( cmd.values && Object.keys(cmd.values).length>0 ){
			const commandnames =  Object.keys(cmd.values).sort().map(c=>{ return cmd.values[c];}).filter(c => {return c;});
			await program.help_of_commands( commandnames, style );
		}else{
			await program.help_main( style );
			if( full ){
				await program.help_full( style );
			}
		}
	});

program
	.command('exit')
	.alias('quit')
	.alias('выход')
	.description('Выход из интерфейса командной строчки')
	.action(async function() {
		program.stop();
		print('Bye Bye..');
		process.exit(0);
	});

program
	.command('do')
	.long('<commandfilename>')
	.description('Выполненить командный файл')
	.note('Команды из файла выполняются одна за одной. Строки, первый значимый (не пробельный) символ которых является знак "#" (решётка) пропускаются.\n\nТаким образом для коментирования текста командного файла можно использовать этот символ (решётка).')
	.action(async function(cmd) {
		const commandfilename = cmd.values['commandfilename'];
		await program.doit( commandfilename, async function( line ){
			await program.parse( program.splitCommand( line ) );
		});
	});

//                                                                -------------------------------

//                                                                =========== START =============
program.start(
	async () =>{
		//onstart
	},
	async () =>{
		//onstop
		await lancheck.stop();
	},
	async (err) =>{
		//onerror
		print( ESC + '31m' +  'ERR: ' + ESC + '0m' +  err.message );
		await lancheck.stop();
	},
	async () =>{
		//title
		print('');
		print( chalk.green(figlet.textSync(program.getName().toUpperCase(), { horizontalLayout: 'full' })) );
		print( '\n' + 'Версия: ' + program.getVersion() );
		print( '\n' + 'Используй команду help, если не знаешь, что делать дальше \n' );
	},
	ENV
);
//                                                                -------------------------------

