'use strict';

const { print }        = require('../modules/print_methods');
const { print_status } = require('../modules/commands_common');
const { check_nodes }  = require('../modules/commands_common');

module.exports = function (conf, methods) {
	return async function ({ status=false, nocolor=false, error=false, changed=false, time=false }) {
		methods.ENV.maycontinue = true;
		await check_nodes( methods, conf.nodes, '', async function( n, treesimbol ){
			if( methods[n.type] ){
				if( (!changed && (!error || !isok(n))) || (n.laststate && n.laststate.isnew) ){

					if( time ) print( ((n.laststate &&  n.laststate.time)?n.laststate.time:' '.repeat(19)) + ' | ', 0 );

					if(!error && !changed){
						//print('-'.repeat(level) + ' ', 0);
						print( treesimbol + String.fromCharCode('0x2500'), 0);
					}

					print( (n.failtype || 'main') + ':', 0 );
					print( (n.level || 0) + ' ', 0 );
					if( typeof(methods[n.type].toString)=='function' ) print( methods[n.type].toString(n), 0);
					if( status ){
						print( ' ... ', 0 );
						print_status(n, { nocolor });
					}
					print('');
				}
			}
			return true; //чтобы перебор узлов не прерывался, необходимо обязательно вернуть true
		});
	};
};

function isok(n){
	return ( !n.laststate || ( n.laststate.status=='ok' ) );
}


/*
				chars = String.fromCharCode('0x2514') + String.fromCharCode('0x2500'); // └ ─
			}else if(i.type==1){
				chars = String.fromCharCode('0x2558') + String.fromCharCode('0x2550'); // ╘ ═
			}else if(i.type==2){
				chars = String.fromCharCode('0x2559') + String.fromCharCode('0x2500'); // ╙ ─
			}else if(i.type==3){
				chars = String.fromCharCode('0x255A') + String.fromCharCode('0x2550'); // ╚ =
			}else{
				chars = String.fromCharCode('0x2514') + String.fromCharCode('0x2500'); //   ─

*/
