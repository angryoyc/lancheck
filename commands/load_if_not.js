'use strict';

const ESC           = '\x1b[';
const fs            = require('fs');
const { promisify } = require('util');
const readFile      = promisify(fs.readFile);
const stat          = promisify(fs.stat);
const { print }     = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ){
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ( {nodelist, nocolor=false, mute=false} ) {
		if(!conf.nodes_path) throw new Error('Ошибка конфигурации: отсутствует параметр nodes_path');
		let confname  = nodelist || conf.default_nodes || 'main';
		let fullpath = conf.nodes_path + '/' + confname + '.json';
		await stat( fullpath );
		conf.current_nodes = confname;
		conf.nodes = eval( '('  + (await readFile( fullpath )) +')' );
		if( !mute ) print( (!nocolor?ESC + '2m':'') + 'узлы загружены' + (!nocolor?ESC + '0m':''));
	};
};

