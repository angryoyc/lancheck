'use strict';

const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');
//print( ESC + '2m' + 'пользователь добавлен' + ESC + '0m');

module.exports = function ( conf, methods, conf_path ) {
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({fio='', nocolor=false}){
		if( !(fio) ) throw new Error('Необходимо указать ФИО');
		if( (conf.admins || []).filter( a =>{ return a.fio==fio } ).length>0 ){
			throw new Error('ФИО уже присутствует');
		}else{
			conf.admins.push({fio, isactive:false, filters:{'*':0}});
			print( (!nocolor?ESC + '2m':'') + 'пользователь добавлен' +  (!nocolor?ESC + '0m':''));
			await save({savemain:true, nocolor});
		}
	};
};

