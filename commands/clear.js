'use strict';

const { print }   = require('../modules/print_methods');
const isObject    = function(a){return (!!a) && (a.constructor === Object)};
const { check_nodes }  = require('../modules/commands_common');
const ESC               = '\x1b[';

module.exports = function ( conf, methods, conf_path ) {
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({savenodes=false, nocolor=false, ok=false, mute=false}){
		methods.ENV.maycontinue = true;
		await check_nodes( methods, conf.nodes, '', async function( n, treesimbol ){
			if( ok ){
				if( !isObject(n.laststate) ) n.laststate={};
				delete n.laststate.message;
				delete n.laststate.isnew;
				n.laststate.status = 'ok';
			}else{
				if( n.laststate ) delete n.laststate;
			}
			return true;
		});
		if( !mute ) print( (!nocolor?ESC + '2m':'') + 'узлы очищены' + (!nocolor?ESC + '0m':''));
		if( savenodes ) await save({savenodes, nocolor});
	};
};

