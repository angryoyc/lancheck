'use strict';

const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({rownum=0, nocolor=false}){
		if( !(rownum>0) ) throw new Error('Номер строки не может быть пустым или нулевым.');
		if( !(rownum<=conf.admins.length) )  throw new Error('Номер строки выходит за диапазон допустимых значений');
		conf.admins.splice(rownum-1, 1);
		print( (!nocolor?ESC + '2m':'') + 'пользователь добавлен' + (!nocolor?ESC + '0m':'') );
		await save({savemain:true, nocolor});
	};
};

