'use strict';

const fs = require('fs');
const { promisify } = require('util');
const readdir = promisify(fs.readdir);
//const { print_object }     = require('../modules/print_methods');
const { print }     = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
//	const save = (require('./save'))( conf, methods, conf_path );
	return async function (){
		if(!conf.nodes_path) throw new Error('Ошибка конфигурации: отсутствует параметр nodes_path');
		let list = (await readdir( conf.nodes_path )).filter( f=>{ return f.match(/\.json$/) }).map( f=>{ return (f.match(/^(.+)\.json$/) || [null,''] )[1]});
		for(let fname of list){
			print(fname);
		}
	};
};

