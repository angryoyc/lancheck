'use strict';

const ESC               = '\x1b[';
const { print }         = require('../modules/print_methods');
const { print_object }  = require('../modules/print_methods');
const { check_nodes }   = require('../modules/commands_common');
const { status_string } = require('../modules/commands_common');
const points            = require('../modules/points');
const { exec }          = require('../modules/commands_common');

const { splitCommand } = require('../modules/commander_async');


/*
const fs               = require('fs');
const { promisify }    = require('util');
const writeFile        = promisify(fs.writeFile);
const isArray          = function(a){return (!!a) && (a.constructor === Array);};
*/

module.exports = function (conf, methods, conf_path){
	//const save = (require('./save'))( conf, methods, conf_path );
	const points_rescan = (require('./points_rescan'))( conf, methods, conf_path );
	return async function ({nocolor=false, force=false, mute=false}){
		methods.ENV['SIGINT'] = function(){
			if( !methods.ENV.maycontinue ){
				print('[ ' + (!nocolor?ESC + '2m':'') + 'выход...' + (!nocolor?ESC + '0m':'')  + ' ]', 0);
				process.exit(0);
			}else{
				print('[ ' + (!nocolor?ESC + '2m':'') + 'ожидаю завершение...' + (!nocolor?ESC + '0m':'')  + ' ]', 0);
			}
			methods.ENV.maycontinue = false;
		}
		methods.ENV.maycontinue = true;
		const list = [];
		// соберем все узлы с новыми проблемами
		await check_nodes(methods, conf.nodes, '', async function( n, treesimbol ){
			if( n.laststate && n.laststate.isnew ){
				let newn = {};
				for( let k in n) if(k!='nodes') newn[k]=n[k];
				newn.level = newn.level || 0;
				list.push( newn );
			}
			return true;
		});

		delete methods.ENV['SIGINT'];

		// Построим очереди для всех активных админов
		let q = [];
		for( let a of conf.admins ){
			if( a.isactive ) q.push( {admin: a, q:[]} );
		}

		//заполним очереди оповещениями

		for( let e of list ){
			for( let a of q ){
				let adminfilter = 65535;
				let failtype = e.failtype || 'main';
				if( failtype in (a.admin.filters || {})){
					adminfilter = a.admin.filters[ failtype ] || 0;
				} else if( '*' in (a.admin.filters || {}) ){
					adminfilter = a.admin.filters[ '*' ] || 0;
				}
				// console.log( failtype, a.admin.fio, adminfilter, adminfilter <= (e.level || 0) );
				if( adminfilter <= (e.level || 0) ) a.q.push( e );
			}
		}

		// выведем очереди сообщений на админов

		await points_rescan({});

		if( !mute ){
			if( points.get().length>0){
				print( 'Найдены точки выхода:' );
				for( let point of points.get() ){
					let ok =  (!nocolor?ESC + '32m':'') + 'ok' + (!nocolor?ESC + '0m':'');
					let notok = (!nocolor?ESC + '31m':'') + 'inaccessible' + (!nocolor?ESC + '0m':'');
					print( point.cmd + ' (' + point.order + ') [ ' + (point.isok?ok:notok) + ' ]' );
				}
			}else{
				print( 'Точки выхода не найдены' );
			}
		}

		for( let row of q ){
			let aCmd = [];
			let cmd;
			//let cmd2;
			for( let point of points.get()){
				for( let media in (row.admin.contacts || {}) ){
					if(/* point.cmd.match( '{' + media + '}')*/ match( point.cmd, media) ){
						//acmd = point.cmd.replace( '{' + media + '}', row.admin.contacts[media] );
						cmd=point.cmd;
						aCmd.push({templ:media, val: row.admin.contacts[media] });
						break;
					}
				}
				if( aCmd.length>0 ){
					for( let sys in (conf.sys || {})){
						if( /*point.cmd.match( '{' + sys + '}' ) */ match(point.cmd, sys) ){
							//cmd2 = (cmd || '').replace( '{' + sys + '}', conf.sys[sys] );
							aCmd.push({templ:sys, val: conf.sys[sys] });
							break;
						}
					}
				}
				if( aCmd.length>1 ) break;
			}
			if( (aCmd.length>1) && (row.q.length>0) ){
				aCmd.push({templ:'myname', val:conf.myname});
				aCmd.push({templ:'subj', val:conf.subj});
				row.acmd = aCmd;
				//const a = cmd.split(' ');
				const a = splitCommand( cmd );
				row.media = (a[0]?a[0]:'').replace(/^\{/,'').replace(/\}$/,'').toUpperCase();
				row.bin = repl(a.shift(), aCmd);
				row.params = a.map( ai =>{
					return repl( ai, aCmd );
				});
				row.cmd_body = row.q.map( n =>{
					return event_sting(methods, n, true);
				});
			}
		}

		if( !mute ){
			let c = 0;
			for( let row of q ){
				c = c + row.q.length;
				if( row.q.length > 0 ){
					print('\n' + row.admin.fio, 0);
					print( ' :: ', 0);
					print( row.bin?(row.media + ': ' + (!nocolor?ESC+'37m':'') + row.params.map( p => { return p.match(/ /)?('"'+p+'"'):p; }).join(' ') + (!nocolor?ESC+'0m':'') ):'-- средства доставки не обнаружены --' );
					print('-'.repeat(150));
					for(let n of row.q){
						print( event_sting(methods, n, nocolor) );
					}
				}
			}
			if(c) print('');
		}

		if( force ){
			for( let row of q ){
				if( row.bin && row.params ){
					print( 'Отправка уведомления: ' + '"' + (nocolor?'':ESC + '36m') + row.bin + ' ' + row.params.map( p => { return p.match(/ /)?('"'+p+'"'):p; }).join(' ') + (nocolor?'':ESC + '0m') + '"'  +  ' событий: ' + row.cmd_body.length + ' всего символов:' + row.cmd_body.join('\n').toString().length, 0 );
					print( ' ... ', 0 );
					try{
						let resp = await exec( row.bin, row.params, row.cmd_body.join('\n'));
						print('[ ok ]');
					}catch(err){
						print( '[ Error: ' + err.message + ']' );
					}
				}
			}
		}

	};
};

function event_sting(methods, n, nocolor ){
	const result = [];
	result.push( ((n.laststate &&  n.laststate.time)?n.laststate.time:' '.repeat(19)) + ' | ' );
	result.push( (n.failtype || 'main') + ':' );
	result.push( (n.level || 0) + ' ' );
	if( typeof(methods[n.type].toString)=='function' ) result.push( methods[n.type].toString(n) );
	result.push( ' ... ' );
	result.push(status_string( n, { nocolor } ));
	return result.join('');
}


function match(cmd, templ){
	let a = cmd.split(/ /);
	a = a.filter( ai => {return ai});
	for( let w of a){
		if( w.match('{' + templ + '}') ) return true;
	}
	return false;
}

function repl(cmd, a){
	let c = cmd;
	for(let pair of a ){
		/*
		if(pair.val.match(/ /)){
			c = c.replace( '{' + pair.templ + '}', '"' + pair.val + '"');
		}else{
			c = c.replace( '{' + pair.templ + '}', pair.val);
		}
		*/
		c = c.replace( '{' + pair.templ + '}', pair.val);
	}
	return c;
}