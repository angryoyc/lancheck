'use strict';

//const { print }        = require('../modules/print_methods');
const { print_table }  = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
	const save = (require('./save'))( conf, methods, conf_path );
	return async function (){
		print_table(
			[
				{column:'#', width: 3, title:'№№'},
				{column:'isactiveshow', width: 1, title:'✔' },
				{column:'fio', width: 50, title:'ФИО'},
				{column:'filters', width: 50, title:'Фильтры'},
				{column:'contacts', width: 50, title:'Контакты'}
			],
			conf.admins.map( a=>{
				return {
					fio: a.fio,
					filters: Object.keys(a.filters || {}).map(c => { return c + ':' + a.filters[c].toString()} ).join(', '),
					contacts: Object.keys(a.contacts || {}).map(c => { return c + (a.contacts[c]?':' + a.contacts[c]:'')} ).join(', '),
					isactiveshow: a.isactive?'✔':'-',
					isactive: a.isactive
				}
			})
//			.sort( (a, b)=>{
//				if( a.isactive && b.isactive ) return 0;
//				if( a.isactive && !b.isactive ) return -1;
//				if( !a.isactive && b.isactive ) return 1;
//				if( !a.isactive && !b.isactive ) return 0;
//			} )
		);
	};
};

/*
			{column:'filecount', width: 6, title:'Файлов', type: 'currency'},
			{column:'bytes', width: 14, title:'Общий размер', type: 'currency'},
			{column:'avgsize', width: 14, title:'Ср. размер', type: 'currency'},
			{column:'plan', width: 10, title:'План'},
			{column:'max_files', width: 13, title:'Файлов (план)', type: 'currency'},
			{column:'max', width: 14, title:'Размер (план)', type: 'currency'},
			{column:'target', width: 70, title:'Цель'}

*/