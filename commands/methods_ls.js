'use strict';

const ESC           = '\x1b[';

const { print }     = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
	return async function ({nocolor=false, full=false}){
		let i = 0
		for( let methodname of Object.keys(methods).sort().filter( m=>{ return m!='ENV' }) ){
			i++;
			print( '✔ ' + (( typeof(methods[ methodname ].desc)=='function' )?methods[ methodname ].desc({nocolor}):'') );
			if( full ){
				print( (( typeof(methods[ methodname ].details)=='function' )?methods[ methodname ].details({nocolor}):'') );
				print('');
			}
		}
	};
};

