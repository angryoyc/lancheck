'use strict';

const { print_object }  = require('../modules/print_methods');
const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');

module.exports = function (conf, methods, conf_path){
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({rownum=0, nocolor=0, tostate=false}){
		if( !(rownum>0) )                   throw new Error( 'Номер строки не может быть пустым или нулевым.' );
		if( !(rownum<=conf.admins.length) ) throw new Error( 'Номер строки выходит за диапазон допустимых значений' );
		let user = conf.admins[rownum-1];
		user.isactive = tostate;
		print( (!nocolor?ESC + '2m':'') + 'админ "' + user.fio + '" ' + (user.isactive?'активирован':'деактивирован') + (!nocolor?ESC + '0m':''));
		await save({savemain:true, nocolor});
	};
};

