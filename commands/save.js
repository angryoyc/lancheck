'use strict';

const fs = require('fs');
const { promisify } = require('util');
const writeFile     = promisify(fs.writeFile);
const stat          = promisify(fs.stat);
const { print }     = require('../modules/print_methods');
const ESC               = '\x1b[';


module.exports = function ( conf, methods, conf_path ) {
	return async function ({savemain=false, savenodes=false, nocolor=false, mute=false}){
		if( savemain ){
			let nodes = conf.nodes;
			delete conf.nodes;
			await writeFile( conf_path, JSON.stringify(conf, null, 4), 'utf8');
			conf.nodes = nodes;
			if(!mute) print( (!nocolor?ESC + '2m':'') +  'конфигурация сохраненa' + (!nocolor?ESC + '0m':''));
		}
		if( savenodes ){
			if(!conf.nodes_path) throw new Error('Ошибка конфигурации: отсутствует параметр nodes_path');
			if( conf.nodes ){
				if(conf.current_nodes){
					let fullpath = conf.nodes_path + '/' + conf.current_nodes + '.json';
					await writeFile( fullpath, JSON.stringify(conf.nodes, null, 4), 'utf8');
					if(!mute) print( (!nocolor?ESC + '2m':'') +'список узлов сохранён' + (!nocolor?ESC + '0m':''));
				}else{
					throw new Error('Неверный формат конфигурационного файла. Отсутствует current_nodes');
				}
			}else{
				throw new Error( 'Нет загруженного списка узлов' );
			}
		}
	};
};

