'use strict';

const { check_nodes }  = require('../modules/commands_common');
const points = require('../modules/points');

module.exports = function (conf, methods) {
	return async function ({}) {
		methods.ENV.maycontinue = true;
		points.clear();
		await check_nodes( methods, conf.nodes, '', async function( n, treesimbol ){
			if( n.type == 'point' ){
				points.push({cmd:n.cmd, order: (n.order || 0), isok: !!(n.laststate && ( n.laststate.status=='ok' )) });
			}
			return true; //чтобы перебор узлов не прерывался, необходимо обязательно вернуть true
		});
		points.sort();
	};
};

