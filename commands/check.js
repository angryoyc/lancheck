'use strict';

const { print_status } = require('../modules/commands_common');
const { check_nodes }  = require('../modules/commands_common');
const { print }        = require('../modules/print_methods');
const fs               = require('fs');
const { promisify }    = require('util');
const writeFile        = promisify(fs.writeFile);
const isArray          = function(a){return (!!a) && (a.constructor === Array);};
const ESC              = '\x1b[';
const moment           = require('moment');

module.exports = function (conf, methods, conf_path){
	const save = (require('./save'))( conf, methods, conf_path );
	const load = (require('./load_if_not'))( conf, methods, conf_path );
	return async function ({showlast=false, nocolor=false, force=false, savenodes=false, mute=false, nodelist}){
		if( !isArray(conf.nodes) && nodelist ){
			await load({ nodelist, mute, nocolor });
		}
		methods.ENV['SIGINT'] = function(){
			if( !methods.ENV.maycontinue ){
				print('[ ' + (!nocolor?ESC + '2m':'') + 'выход...' + (!nocolor?ESC + '0m':'')  + ' ]', 0);
				process.exit(0);
			}else{
				print('[ ' + (!nocolor?ESC + '2m':'') + 'ожидаю завершение...' + (!nocolor?ESC + '0m':'')  + ' ]', 0);
			}
			methods.ENV.maycontinue = false;
		}
		methods.ENV.maycontinue = true;
		await check_nodes( methods, conf.nodes, '', async function( n, treesimbol ){
			if( !mute) print( treesimbol + String.fromCharCode('0x2500'), 0);
			if( methods[n.type] ){
				if( !mute) print( (n.failtype || 'main') + ':', 0 );
				if( !mute) print( (n.level || 0) + ' ', 0 );
				if( typeof(methods[n.type].toString)=='function' ) if( !mute) print( methods[n.type].toString(n) + ' ... ', 0 );
				if( typeof(methods[n.type].check)=='function' ){
					let laststate = await (methods[n.type].check)(n);
					if( n.laststate && ( n.laststate.status=='ok' ) && ( laststate && laststate.status!='ok') ){
						n.laststate = laststate;
						n.laststate.isnew = true;
					}else{
						n.laststate = laststate;
						n.laststate.isnew = false;
					};
				}else{
					n.laststate = {};
					n.laststate.isnew = false;
				}
				n.laststate.time = moment().format("YYYY-MM-DD HH:mm:ss");
			}
			if( !mute) print_status(n, { nocolor });
			if( !mute) print('');
			const recursion = force || ( (( n.laststate && n.laststate.status) || '').toLowerCase()=='ok' ) || (!n.laststate) ;
			if( !recursion ){
				dropLaststate( n.nodes );
			}
			return ( recursion );
		});
		//points.sort();
		delete methods.ENV['SIGINT'];
		if( savenodes ) await save({savenodes, nocolor, mute});
	};
};

function dropLaststate( nodes ){
	for( let n of nodes ){
		delete n.laststate;
		if( n.nodes && n.nodes.length>0 ){
			dropLaststate( n.nodes );
		}
	}
}
