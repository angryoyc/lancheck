'use strict';

const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({nodes_path, nocolor=false}){
		if(nodes_path) conf['nodes_path'] = nodes_path;
		print( (!nocolor?ESC + '2m':'') + 'Свойство ' + 'nodes_path' + ' установлено' + (!nocolor?ESC + '0m':''));
		await save({savemain:true, nocolor});
	};
};

