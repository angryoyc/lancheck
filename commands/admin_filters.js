'use strict';

let save;

const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');

module.exports = function ( conf, methods, conf_path ) {
	save = (require('./save'))( conf, methods, conf_path );
	return async function ({rownum=0, filter='', level=null, nocolor=false}){
		if( !(rownum>0) )                   throw new Error( 'Номер строки не может быть пустым или нулевым.' );
		if( !(rownum<=conf.admins.length) ) throw new Error( 'Номер строки выходит за диапазон допустимых значений' );
		if( !filter )                       throw new Error( 'Тип сбоя (failtype) - обязательный параметр. Можно использовать "*" для всех типов' );
		if( level==null ){
			if(filter.match(/\:/)){
				let m = filter.match(/^(.+)\:(\d+)$/);
				if(m){
					filter = m[1];
					level = parseInt(m[2]);
					await setfilter(conf, nocolor, rownum, filter, level);
				}else{
					throw new Error('Не распознан формат failtype');
				}
			}else{
				//Удаление
				await delfilter(conf, nocolor, rownum, filter);
			}
		}else{
			await setfilter(conf, nocolor, rownum, filter, level);
		}
	};
};

async function setfilter(conf, nocolor, rownum, filter, level){
	level = parseInt(level || 0);
	let user = conf.admins[rownum-1];
	if( !user.filters ) user.filters={};
	user.filters[filter]=level;
	print( (!nocolor?ESC + '2m':'') + 'фильтр установлен' + (!nocolor?ESC + '0m':''));
	await save({savemain:true, nocolor});
}

async function delfilter(conf, nocolor, rownum, filter){
	let user = conf.admins[rownum-1];
	delete user.filters[filter];
	print( (!nocolor?ESC + '2m':'') + 'фильтр отменён' + (!nocolor?ESC + '0m':'') );
	await save({savemain:true, nocolor});
}
