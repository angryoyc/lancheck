'use strict';

const { print_object }  = require('../modules/print_methods');

module.exports = function (conf, methods, conf_path){
	return async function ({rownum=0}){
		if( (rownum>0) && (rownum<=conf.admins.length) ){
			print_object( conf.admins[rownum-1] );
		}else{
			throw new Error('Номер строки вне диапазона');
		}
	};
};

