'use strict';

const { print_object }  = require('../modules/print_methods');
const ESC               = '\x1b[';
const { print }        = require('../modules/print_methods');
const isObject    = function(a){return (!!a) && (a.constructor === Object)};

module.exports = function (conf, methods, conf_path){
	const save = (require('./save'))( conf, methods, conf_path );
	return async function ({rownum=0, nocolor=0, contname='', contval=''}){
		if( !(rownum>0) )                   throw new Error( 'Номер строки не может быть пустым или нулевым.' );
		if( !(rownum<=conf.admins.length) ) throw new Error( 'Номер строки выходит за диапазон допустимых значений' );
		let admin = conf.admins[rownum-1];
		if( !isObject( admin.contacts) ) admin.contacts={};
		if( !contname ) throw new Error('Необходимо указать тип средства связи: email|phone|sms|...');
		if( contval ){
			// задание средства связи
			admin.contacts[contname] = contval;
			print( (!nocolor?ESC + '2m':'') + 'админу "' + admin.fio + '" задано средство связи ' + contname + (!nocolor?ESC + '0m':''));
		}else{
			// удаление средства связи
			delete admin.contacts[contname];
			print( (!nocolor?ESC + '2m':'') + 'админу "' + admin.fio + '" удалено срдство свзяи ' + contname + (!nocolor?ESC + '0m':''));
		}
		await save({savemain:true, nocolor});
	};
};

