# LANCHECK

Утилита проверки состояния элементов сети.

# Install / Установка


```bash
npm install git+ssh://serg@git.settv.ru:1025/opt/git/lancheck.git
```

or / или

```bash
git clone https://gitlab.com/angryoyc/lancheck.git
npm i
```

# HELP / Помощь

## [Command reference / Справочник команд](./HELP.md)

*Приму в дар помощь по переводу этого руководства на здоровый и современный английский язык!*

*Ландан из зе кэпитал оф зе грэйт британ просьба не беспокоить*

*Приму в дар помощь по исправлению орфографических ошибок и прочим редакторским правкам текстов на русском языке*

## Режимы использования
Утилита может быть использована как в интерактивном режиме, так и из командной строки sh|bash|...

То есть, можно запустить утилиту (`$ lancheck`) из шела, а дальше выполнять команду уже в её среде

```
$ lancheck

  _          _      _   _    ____   _   _   _____    ____   _  __
 | |        / \    | \ | |  / ___| | | | | | ____|  / ___| | |/ /
 | |       / _ \   |  \| | | |     | |_| | |  _|   | |     | ' / 
 | |___   / ___ \  | |\  | | |___  |  _  | | |___  | |___  | . \ 
 |_____| /_/   \_\ |_| \_|  \____| |_| |_| |_____|  \____| |_|\_\
                                                                 

Версия: 0.0.3

Используй команду help, если не знаешь, что делать дальше 

lancheck> help
```
Либо выполнять команды прямо из шелла :

```bash
$ lancheck start main -s -a
```

так же имеется возможно выполнять пакеты команд сохранённые в отдельном файле. Эта возможность может быть полезна, если у вас имеется несколько наборов тестируемых узлов (например, набор "all" или "computers" или "voip") и  несколько режимов их тестирования, то 
удобно подгтовить небольшие командные файлы, в которых будут описаны все необходимые действия. Например, файл **test-voip.lancheck** вида:

```
#!/usr/bin/lancheck do

load voip
go -f
save
ls -c -s
```

загрузит список узлов "comps", запустит их проверку в режиме --force когда тестируются все узлы списка, включая те, которые зависят от узлов не прошедших проверку. (см. об этом ниже), результат тестирования будет сохранён, а на экран выведен список узлов, которые с момента последней проверки потеряли статус "ok"

## Порядок действий

### Формирование списка тестируемых узлов

Предполагается, что перед использованием утилиты, будут созданы списки узлов, подлежащих тестированию. Формат списков - JSON. Каждый список должен содержать массив объектов (узлов), имеющих следующие обязательные свойства:

`type` - одно из следующих: ping, webreq, df

`note` - описание теста. Например, "Проверка доступности главного маршрутизатора"

все прочие параметы узла зависят от его типа. Например, тип *ping* предполагает свойство "ip" - которое должно содержать DNS-имя или IP-адрес тестируемого узла, а так же необязательный параметр "threshold", задающий пороговое значение потерь, при котором тест данного узла будет считаться проваленым.

полный список всех типов и параметров можно посмотреть [здесь](./METHODS.md)

Также каждый из узлов может содержать свойство `nodes` - содержащий список зависимых узлов. При тестировании, в случае, если тестирование узла прошло неудачно, то переход к тестированию зависимых узлов не происходит (кроме случая использования опции -f).
Используя это свойство, списки узлов можно составлять таким образом, чтобы тесты, которые заведомо не могут быть пройдены (например, если не отвечает основной роутер, то очевидно, бессмысленно пытаться тестировать узлы, находящиеся за ним) были пропущены. 
Кроме того, в случае сложной сети, имеющей несколько выходов в сеть интернет, такой способ формирования списков узлов позволяет выбрать работоспособный на момент теста способ доставки оповещений до админов. 
То есть, если в результате тестов была выявлена неработоспособность одного из шлюзов, то метод оповещнияя, зависящий от этого шлюза уже не будет использоваться. В этом случае программа как бы ищет лазейку, через которую админу могут быть отправлены оповещения о сбоях.

Для каждого из узлов могут быть указаны два необязательные параметра: 

`failtype` - тип сбоя. Позволяет разделить ответственность за участки сети между админами. Например, отправлять оповещение о сбоях телефонии, человеку, который за это отвечает и не тревожит тех, кто занимается, скажем, видеонаблюдением или 1с.

`level` - уровень сбоя. Позволяет задать уровень сбоя. ( Чем больше level, тем выше важность события ). Например, отсутствие пингов до IP-телефона сотрудника может расцениваться как "незначительный" ( level=0 ) и сообщение о таком событии будет включено только в общий лог, а оповещение админу отправлено не будет, а вот выход из строя одного из серверов может расцениваться как "критическое" (level=3) событие и информирование о нём будет произведено в любом случае, даже во время DND.

при помощи этого свойства можно создавать мультиуровневые тестирования. Например, можно создать два узла для одного и того же сервера с типом df (контроль свободного места на блочном устройстве). 
Для одного узла будет указан предел (threshold) равный 50% использованного пространства и level=0 и этот тест будет генерирвать только предупреждение, если заполнено более половины устройства, а второй может содержать threshold=90% и иметь level=3 - то есть, сообщение об этом сбоее будет иметь наивысший приоретет и будет отправляться админу в любом случае.

### Формирование списка администраторов

пишу. скоро будет.

Управление списком администаторов производится командами начинающимися на `admin`. Эта команда без параметров вернёт список администраторов. 

`admin`  - список.

`addadmin "Суходрищев Иван Сергеевич"` - добавить администратора

`admin 35` - вывести детальные сведения об администраторе, указанного в строке `35` списка, выводимого по команде `admin`

`admin 35 del` - удалить администратора, указанного в строке `35` списка, выводимого по команде `admin`

`admin 3 constr voip 3` - установить ограничение на отправку оповещений о сбоях типа 'voip' менее чем с уровнем 3. ДопустИм формат:

```
lancheck> admin 3 constr voip:3
конфигурация сохраненa
```

Если при указании ограничения, указать только тип сбоя (failtype) ( и без двоеточия ) то соответствующее ограничение будет снято совсем.

Допускается использование вместо failtype символа "*" - обозначающего любой тип сбоя. Например команда

```
lancheck> admin 3 constr *:3
конфигурация сохраненa
```

Установит ограничение для всех типов не ниже 3.


### Формирование командных файлов

Сформируйте текстовый файл сценария с командами lancheck, например, как в примере выше. Убедитесь, что он доступен по чтению для пользователя, который будет запускать тесты. 

Далее у вас есть на выбор два способа исполнения этих сценариев

1. Чере команду `cat`
```bash
$ cat test-voip.lancheck | lancheck
```

2. Через превращение самого файла-сценария в исполняемый файл. Для этого, установите флаг, разрешающий исполнение сценария для соответствующего пользователя и группы:
```bash
$ chmod 550 test-voip.lancheck
$ chown serg:oper test-voip.lancheck
```
где 

`test-voip.lancheck` - файл сценария

`serg` - пользователь под которым будет исполняться тест

`oper` - группа пользователей под которым ещё может быть исолнен этот тест

Второй способ более предпочтительный, так как после выполнения описанных выше действий файл может быть непосредственно исполнен из командной строки шела:

```bash
$ ./test-voip.lancheck
```
или включен в cron:

```bash
$ crontab -e
```

где указать, например:

```
...
*/10 * * * * /usr/scripts/test-voip.lancheck | logger -t lancheck
...
```

для запуска тестрирования VoIP-телефонии каждые 10 минут, с выводом результатов в syslog с тэгом 'lancheck'

Впрочем, выполнить тестирование, записать результат и запустить оповещение можно и одной строкой:

```
...
*/10 * * * * /usr/bin/lancheck start main -s -a | logger -t lancheck
...
```

так что необходимости в файле сценария может и не быть


### Возможности расширения

Функционал утилиты может быть расширен за счёт добавления новых методов тестирования и за счёт добавления новых средств доставки сообщений. 

Для добавления нового метода тестирования достаточно добавить JS-файл в папку methods. Структура и требавания к этому файлу описана  [здесь]( ./METHODS_EXTENDS.md ). После добавления нового метода рекомендуется выполнить из корневой директории этого проекта

```bash
lancheck methods -f -n > ./METHODS.md
```

Для использования дополнительного способа доставки необходимо иметь консольную утилиту, принимающую тело сообщения на STDIN. 
При отсутствии таковой, необходимо создать соответствующий скрипт-обёртку.
Например, вы можете написать простой скрипт для отправки SMS через один из соответствующих сервисов, который будет принимать адрес получателя в первом параметре командной строки, а тело сообщения на STDIN.
Скрипт должен завершиться с нулевым кодом, в случае успешного выполнения, или ненулевым кодом, в случае возникновения ошибки, при этом в STDERR должно быть отправлено описание возникшей проблемы.
Для того, чтобы использовать этот скрипт достаточно выполнить два шага.
1. В файле `config.json` в разделе *sys* необходимо указать `"<alias>": "<path-to-script>"`. Пример: `"sendsms": "/opt/sendsmsjs/sendsms.js"`
2. Добавить в файл списка узлов узел типа `point`. Пример:
```json
...
    {
        "type": "point",
        "cmd": "{sendsms} {phone} {subj} {myname}",
        "order": 0
    }
...
```

где

{sendsms} - alias утилиты, указанный в конфигурационном файле на первом шаге.

{phone}   - будет заменен на контакт типа `phone` из настроек админа

{subj}    - будет заменен на текст из параметра `subj` файла настроек config.json. Предполагается использование этого параметра для определения темы сообщения. Например: "Оповещение об отказе".

{myname}  - будет заменен на текст из параметра `myname` файла настроек config.json. Предполагается использование этого параметра для указания имени хоста с которого производится тестирование. Например: "admin.attava.com"

Параметр order в настройках узла, определяет порядок сортировки точек доставки, если в результате обхода дерева узлов, 
будут выявлены более одного способа доставки, при этом будет использована точка выхода с наименьшим значением order.
Таким образом, вы можете настроить сразу несколько способов доставки собщений. 
Например, штатный способ доставки может быть через E-Mail (скажем, с использованием скрипта [ sendmailjs ](https://gitlab.com/angryoyc/sendmailjs), 
а запасной через отправку SMS через GPRS-модем, подключенный непосредственно к тому хосту, на котором будет запускаться скрипт доставки. 
Таким образом можно экномить ценный (платный) ресурс SMS, используя его лишь в случае недоступности сети.
Если для админа не указано соответствующий тип контакта (здесь в примерах это phone ), то данная точка выхода не будет использована для доставки сообщений данному админу.



